from common.recursive_folders import recursive_folders
import pandas as pd
import re

# VARIÁVEIS A SEREM FORNECIDAS
dic_expressoes = {
    'saude_leitos':[r'sa.de',r'UTI|hospital',r'leito|vagas'],
    'saude_medicamentos':[r'saude',r'medicamento|cloroquina|tratamento'],
    'penal_hc':[r'habeas corpus'],
    'penal_recomendacao_62':[r'penal|criminal',r'recomendação.{,25} 62'],
    'penal_aud_custodia':[r'penal|criminal', r'audiência.{,5}custódia'],
    'moradia_aluguel':[r'despejo|resivional|desconto',r'aluguel'],
    'moradia_propriedade':[r'financiamento habitacional|financiamento imobiliário|minha casa minha vida|MCMV'],
    'moradia_condomínio':[r'condomínio|condominial'],
    'moradia_posse':[r'reintegração de posse|imissão na posse|interdito proibitório'],
    'airbnb':[r'airbnb'],
    'preco_excessivo':[r'preço.{,20}(abusivo|excessivo)'],
    'preco_congelamento':[r'(congelamento|manutenção).{,10}preço'],
    'mensalidade':[r'(reajuste|revisão|desconto).{,25}mensalidade'],
    'ensino':[r'escola|ensino|faculdade'],
    'equilibrio_financeiro':[r'equilíbrio.{,25}financeiro'],
}
path = '/home/deathstar/Dados/Extração_covid'
nome_relatorio = 'relatório_booleano_covid19.csv'

rec = recursive_folders()
paths = [i for i in rec.find_files(path) if i[-3:] == 'csv']
dic_processos = {}
for f in paths:
    print(f)
    try:
        df = pd.read_csv(f)
    except Exception as e:
        print(e)
        continue
    for _, row in df.iterrows():
        if row['numero_processo'] not in dic_processos:
            dic_processos[row['numero_processo']] = {k:0 for k in dic_expressoes}
            dic_processos[row['numero_processo']]['tribunal'] = row['tribunal']
        for k,v in dic_expressoes.items():
            found = True
            for exp in v:
                if not re.search(exp,row['texto_publicacao'],flags=re.S|re.I):
                    found = False
                    break
            if found:
                dic_processos[row['numero_processo']][k] = 1
rows = []
for num, dic_vars in dic_processos.items():
    dic_aux = {
        'numero_processo':num
    }
    for var,res in dic_vars.items():
        dic_aux[var] = res
    rows.append(dic_aux)
df_new = pd.DataFrame(rows)
df_new.to_csv(nome_relatorio,index=False)
