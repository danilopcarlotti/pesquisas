import re
import spacy
from regex_classifier_legal_phrases import palavras_interesse

def dicionario_frases_tipos():
    dic_tipos_frases = {}
    for frase, tipo in palavras_interesse.items():
        if tipo not in dic_tipos_frases:
            dic_tipos_frases[tipo] = []
        dic_tipos_frases[tipo].append(r'{}'.format(frase))
    return dic_tipos_frases

def classifier_legal_phrases_regex(phrase):
    dic_tipos_frases = dicionario_frases_tipos()
    for tipo, conj_exp in dic_tipos_frases.items():
        for exp in conj_exp:
            if re.search(exp, phrase, re.I):
                return tipo
    return 'argumento'

def break_sentences(text, nlp):
    text = re.sub(r'\s+',' ',text)
    text = re.sub(r'art\.','art',text)
    doc = nlp(text)
    return [sent.text for sent in doc.sents]

def classify_sentences_text(text, nlp=None):
    if not nlp:
        nlp = spacy.load('pt_core_news_sm')
    dicionario_frases = {
        'argumento':[],
        'decisao':[],
        'pedido':[],
        'fato':[],
        'lei':[]
    }
    for sentence in break_sentences(text, nlp):
        dicionario_frases[classifier_legal_phrases_regex(sentence)].append(sentence)
    return dicionario_frases
