# Pesquisas

Este é um repositório aberto para auxílio em pesquisas acadêmicas que utilizem técnicas de processamento de textos.

O tema das pesquisas desenvolvidas com este repositório é o processamento de textos jurídicos.

Contribuições são bem vindas.