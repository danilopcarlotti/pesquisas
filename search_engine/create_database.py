import pandas as pd, sys, numpy as np, pickle, pymongo, re, os
from main_class import main_class
from mongo_url import mongo_url
from sklearn.feature_extraction.text import HashingVectorizer
sys.path.append(os.path.dirname(os.path.dirname(os.getcwd())))
from pesquisas.common.recursive_folders import recursive_folders
from pesquisas.common_nlp.textNormalization import textNormalization

class create_database(main_class):
    def __init__(self):
        self.files = [i for i in self.rec.find_files(self.PATH) if i[-3:] == 'csv']
        self.SEP = ';'
        self.ENCODING = 'latin-1'

    def create_db(self):
        for f in self.files:
            df = pd.read_csv(f,sep=self.SEP,encoding=self.ENCODING)
            df[self.COLUMN_SOURCE] = df[self.COLUMN_SOURCE].astype(str)
            session = self.myclient.start_session()
            for _, row in df.iterrows():
                normal_text = None
                if len(row[self.COLUMN_SOURCE]) > 5:
                    # if not re.search(r'',row[COLUMN_SOURCE],re.I):
                    #     normal_text = ' '.join(txtN.normalize_texts(row[COLUMN_SOURCE],one_text=True))
                    normal_text = ' '.join(self.txtN.normalize_texts(row[self.COLUMN_SOURCE],one_text=True))
                else:
                    normal_text = ' '.join(self.txtN.normalize_texts(row[self.ALTERNATE_COLUMN_SOURCE].split('-')[1],one_text=True))
                if normal_text:
                    X = [float(i) for i in list(self.vectorizer.transform([normal_text]).toarray()[0])]
                    dic_aux = {}
                    for c in df.columns:
                        dic_aux[c] = row[c]
                    dic_aux['vetor'] = X
                    self.mydb[self.COLLECTION].insert_one(dic_aux)
            self.myclient.admin.command('refreshSessions', [session.session_id], session=session)

